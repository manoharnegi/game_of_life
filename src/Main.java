import java.util.Arrays;
import java.util.Scanner;
import edu.princeton.cs.introcs.*;

class GameOfLiveSimulation {

    private enum Generations {
        GENERATION_A,
        GENERATION_B
    }

    private int[][] universalGenerationA;
    private int[][] universalGenerationB;

    private long    generationNumber = 1;

    private Generations currentGeneration = Generations.GENERATION_A;

    private void randomFirstGeneration(int maxPopulation) {
        int population = 0;

        for (int i = 0; i < universalGenerationA.length; i++)
            for (int j = 0; j < universalGenerationA[i].length; j++) {

                if (population < maxPopulation) {

                    if (Math.round(Math.random()) == 1) {
                        universalGenerationA[i][j] = 1;
                        population++;
                    }

                } else {
                    universalGenerationA[i][j] = 0;
                }

            }
    }
    private void printCurrentGeneration() {
        int[][] outArray   = this.universalGenerationA;

        switch (this.currentGeneration) {
            case GENERATION_A:
                outArray = this.universalGenerationA;
                break;

            case GENERATION_B:
                outArray = this.universalGenerationB;
                break;
        }

        for (int i = 0; i < outArray.length; i++) {
            System.out.println(Arrays.toString(outArray[i]));
        }
        System.out.println();
    }
    private void calculateNextGeneration() throws RuntimeException {
        int[][] curGenArray = (currentGeneration == Generations.GENERATION_A ? universalGenerationA : universalGenerationB);
        int[][] outGenArray = (currentGeneration == Generations.GENERATION_A ? universalGenerationB : universalGenerationA);

        for (int i = 0; i < outGenArray.length; i++)
            System.arraycopy(curGenArray[i], 0, outGenArray[i], 0, curGenArray[i].length);

        for (int i = 0; i < curGenArray.length; i++) {
            for (int j = 0; j < curGenArray[i].length; j++) {

                int neighbors = curGenArray[i][j] == 0 ? 0 : -1;
                for (int start_i = i - 1; start_i <= i + 1; start_i++) {
                    if (start_i < 0 || start_i >= curGenArray.length)
                      continue;

                    for (int start_j = j - 1; start_j <= j + 1; start_j++) {
                      if (start_j < 0 || start_j >= curGenArray[i].length)
                          continue;

                      if (curGenArray[start_i][start_j] == 1)
                          neighbors++;
                    }
                }

                if (curGenArray[i][j] == 0 && neighbors == 3)
                    outGenArray[i][j] = 1;

                if (curGenArray[i][j] == 1 && (neighbors > 3 || neighbors < 2))
                    outGenArray[i][j] = 0;

            }
        }

        if (currentGeneration == Generations.GENERATION_A)  {
            currentGeneration = Generations.GENERATION_B;
            for (int i = 0; i < outGenArray.length; i++)
                System.arraycopy(outGenArray[i], 0, universalGenerationB[i], 0, outGenArray[i].length);
        }
        else {
            currentGeneration = Generations.GENERATION_A;
            for (int i = 0; i < outGenArray.length; i++)
                System.arraycopy(outGenArray[i], 0, universalGenerationA[i], 0, outGenArray[i].length);
        }

        generationNumber++;
    }
    private void drawScene() {
        // clear map
        StdDraw.clear();

        // draw grid
        for (double i = 0; i <= 1; i += 0.1) {
            StdDraw.line(.0 + i, .0, .0 + i, 1);
            StdDraw.line(.0, .0 + i, 1, .0 + i);
        }

        // draw array
        int outArray[][] = (currentGeneration == Generations.GENERATION_A ? universalGenerationA : universalGenerationB);
        for (int i = 0; i < outArray.length; i++)
            for (int j = 0; j < outArray[i].length; j++)
                if (outArray[i][j] == 1) {
                    StdDraw.filledCircle(.0 + (i + 1)*1.0/10 - 0.05, .0 + (j + 1)*1.0/10  - 0.05, 0.03);
                }

        // draw number
        StdDraw.text(.2, .001, "Generation: " + this.generationNumber);

        // wait for a 1 sec
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void run(int simulationCount, int uniWidth, int uniHeight, int firstGenC) {
        universalGenerationA = new int[uniWidth][uniHeight];
        universalGenerationB = new int[uniWidth][uniHeight];

        randomFirstGeneration(firstGenC);
        for (int i = 0; i < simulationCount; i++) {
            // printCurrentGeneration();
            drawScene();

            try {
                calculateNextGeneration();
            }
            catch(RuntimeException ex) {
                ex.printStackTrace();
            }
        }

    }


}

public class Main {

    public static void main(String[] args) {
//        System.out.println("<Simulate generation count (-1 - inf)> <Universal width> <Universal height> <First generation count>");

        new GameOfLiveSimulation().run(50, 10, 10, 65);
//        Scanner in = new Scanner(System.in);
//
//        int simCount  = in.nextInt();
//        int uniWidth  = in.nextInt();
//        int uniHeight = in.nextInt();
//        int firstGenC = in.nextInt();
//
//        new GameOfLiveSimulation().run(simCount, uniWidth, uniHeight, firstGenC);
    }

}
